from . import httpx, xmletree
from typing import Mapping, Tuple
from parser.step_three import get_resource as three


def parsing_resource(root: xmletree.Element,
                     topic: str) -> Tuple[Mapping[str, int], str]:
    """ Парсинг данных """
    information = {}
    name = ''
    for data in root:
        region = data.find('name').text
        for item in data.find('statistics'):
            code = item.find('code').text
            if code == topic:
                name = item.find('name').text
                value = item.find('value').text
                information[region] = int(value)
    return information, name


def get_resource(topic) -> Tuple[Mapping[str, int], str]:
    """ Получение данных """
    response = httpx.get(three())
    element = xmletree.fromstring(response.content)
    return parsing_resource(element, topic)
