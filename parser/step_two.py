import urllib.request
from . import xmletree
from parser.step_one import get_resource as one


def parsing_resource(root) -> str:
    """ Парсинг данных """
    for tag in root:
        element = xmletree.fromstring(tag)
        source = ''
        for dataversion in element.find('data'):
            source = dataversion
        return source.find('source').text


def get_resource() -> str:
    """ Получение данных """
    page = urllib.request.urlopen(one())
    return parsing_resource(page)
