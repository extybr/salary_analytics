from . import httpx, xmletree
from parser.step_two import get_resource as two


def parsing_resource(root: xmletree.Element) -> str:
    """ Парсинг данных """
    for data in root:
        for item in data.find('regions'):
            return item.find('link').get('resource').split('#')[0]


def get_resource() -> str:
    """ Получение данных """
    response = httpx.get(two())
    element = xmletree.fromstring(response.content)
    return parsing_resource(element)
