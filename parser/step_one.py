from . import httpx, xmletree

RESOURCE = "http://opendata.trudvsem.ru"


def parsing_resource(root: xmletree.Element) -> str:
    """ Парсинг данных """
    for tag in root:
        if tag.tag == "meta":
            for item in tag.findall("item"):
                if item.find('link').text.endswith('districts'):
                    result = item.find('link').text
                    return result


def get_resource() -> str:
    """ Получение данных """
    response = httpx.get(RESOURCE)
    element = xmletree.fromstring(response.content)
    return parsing_resource(element)
