import matplotlib.pyplot as plt
from typing import List, Tuple
from datetime import datetime
from parser.step_four import get_resource as four

TOPIC = 'MEDIUM_SALARY_DIFFERENCE'


def output_data() -> List[Tuple[str, int]] and str:
    """ Вывод данных """
    information, label = four(TOPIC)
    data = list(information.items())
    dt = sorted(data, key=lambda x: x[1])
    for city, salary in dt:
        print('{}: {}'.format(city, salary))
    return dt, label


def print_graph() -> None:
    """ Вывод графиков """
    dt, label = output_data()
    plt.title(label, fontsize=16)
    plt.xlabel('region', fontsize=14)
    plt.ylabel(TOPIC)
    plt.xticks(rotation=0)
    plt.bar(list(range(1, len(dt) + 1)), list(i[1] for i in dt))
    image = 'myplot.png'
    plt.savefig(image)
    # plt.show()

    text = ''
    for data in range(len(dt)):
        text += ("<br><font color='blue' size='5'>{}"
                 "</font> - <font color='green' size='5'>{}"
                 "</font>: <font color='red' size='5'>{}"
                 "</font>".format(data + 1, dt[data][0], dt[data][1]))

    time = "<br><br>{}".format(datetime.now())[:-7]

    full_text = (f"""<!DOCTYPE html><html><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Статистика</title></head><body><figure><div align="center">
    <h3>Статистика зарплат в России по регионам. Данные
    <a href="https://rostrud.gov.ru"> rostrud.gov.ru</a> и
    <a href="https://trudvsem.ru"> trudvsem.ru</a></h3>
    <img src="{image}" height="500"/></div></figure><figcaption>
    <p align="center">{text}{time}</p></figcaption></body></html>""")

    with open('index.html', 'w', encoding='utf-8') as index:
        index.write(full_text)


if __name__ == "__main__":
    print_graph()
